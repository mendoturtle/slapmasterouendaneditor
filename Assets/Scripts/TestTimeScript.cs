﻿using UnityEngine;
using System.Collections;

public class TestTimeScript : MonoBehaviour {

	private Vector3 initPos;
	private float MainCharSpeed = 5.0f;

	// Use this for initialization
	void Start () {
		initPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float totalTime = GameObject.Find("ProgressBarObj").GetComponent<ProgressBar>().GetTime();
		float numUpdates = totalTime / Time.deltaTime;

		transform.position = new Vector3(initPos.x, initPos.y, initPos.z + (numUpdates * Time.deltaTime*MainCharSpeed));
	}
}
