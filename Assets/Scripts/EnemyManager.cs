﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {

	public List<GameObject> enemies;

	List<GameObject> enemiesToDelete;

	public GameObject TapEnemy;
	public GameObject HorEnemy;
	public GameObject VertEnemy;
	private int enemyName = 0;

	// Use this for initialization
	void Awake () {
		enemies = new List<GameObject>();
		enemiesToDelete = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {

		if(enemiesToDelete.Count > 0)
		{
			foreach(GameObject enemy in enemiesToDelete)
			{
				Destroy(enemy);
			}

			enemiesToDelete.Clear ();
		}

		if (Input.GetKeyDown(KeyCode.W))
		{
			CreateEnemy(3, GameObject.Find("MainCharacter").transform.position);
		}
		else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
		{
			CreateEnemy(2, GameObject.Find("MainCharacter").transform.position);
		}
		else if (Input.GetKeyDown(KeyCode.S))
		{
			CreateEnemy(1, GameObject.Find("MainCharacter").transform.position);
		}
	}

	public void CreateEnemy(int Type, Vector3 position)
	{
		if (Type == 3)
		{
			GameObject obj = Instantiate(VertEnemy) as GameObject;
			obj.transform.position = position;
			obj.name = obj.name + " " + enemyName;
		}
		else if (Type == 2)
		{
			GameObject obj = Instantiate(HorEnemy) as GameObject;
			obj.transform.position = position;
			obj.name = obj.name + " " + enemyName;
		}
		else if (Type == 1)
		{
			GameObject obj = Instantiate(TapEnemy) as GameObject;
			obj.transform.position = position;
			obj.name = obj.name + " " + enemyName;
		}
		enemyName++;
	}

	public void AddEnemy(GameObject enemy)
	{
		enemies.Add (enemy.transform.parent.gameObject);
		GameObject.Find("Browser/Browser_Levels").GetComponent<BrowserLevels>().SetDirty();
	}

	public void RemoveEnemy(GameObject enemy)
	{
		Debug.Log("LIST  - " + enemies.Count);
		enemiesToDelete.Add (enemy);
		enemies.Remove(enemy);

		for(int i = 0; i < enemies.Count; ++ i)
		{
			Debug.Log(enemies[i]);
		}

		GameObject.Find("Browser/Browser_Levels").GetComponent<BrowserLevels>().SetDirty();
		
		Debug.Log("LIST AFTER - " + enemies.Count);
	}

	public void RemoveAllEnemies()
	{
		for(int i = 0; i < enemies.Count; ++ i)
		{
			RemoveEnemy(enemies[i]);
			--i;
		}

		enemies.Clear();
		enemyName = 0;
	}
}
