﻿using UnityEngine;
using System.Collections;

public class PlayPauseScript : MonoBehaviour {

	public Texture2D pauseTexture;
	public Texture2D playTexture;
	private GUITexture selfTexture;
	private bool isPaused;
	GameObject progressBar;


	void Start()
	{
		selfTexture = GetComponent<GUITexture>();
		
		isPaused = !GameObject.Find("Managers").GetComponent<AudioSource>().playOnAwake;

		progressBar = GameObject.Find("ProgressBarObj");

		if(isPaused)
		{ 
			PauseEverything();
		}
		else
		{
			UnpauseEverything();
		}
	}
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseUp()
	{
		if(!isPaused)
		{ 
			PauseEverything();
		}
		else
		{
			UnpauseEverything();
		}
	}

	public void PauseEverything(bool shouldPropagate = true)
	{
		isPaused = true;
		selfTexture.texture = playTexture;
		Time.timeScale = 0.0f;

		if(shouldPropagate)
		{
			progressBar.SendMessage("PauseEverything", false);
		}
	}

	public void UnpauseEverything(bool shouldPropagate = true)
	{
		isPaused = false;
		selfTexture.texture = pauseTexture;
		Time.timeScale = 1.0f;

		if(shouldPropagate)
		{
			progressBar.SendMessage("UnpauseEverything", false);
		}
	}
}
