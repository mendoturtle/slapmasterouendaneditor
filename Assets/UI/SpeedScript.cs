﻿using UnityEngine;
using System.Collections;

public class SpeedScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.Find("Normal").guiText.material.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown()
	{
		GameObject.Find("Fastest").guiText.material.color = Color.white;
		GameObject.Find("Fast").guiText.material.color = Color.white;
		GameObject.Find("Normal").guiText.material.color = Color.white;
		GameObject.Find("Slow").guiText.material.color = Color.white;
		GameObject.Find("Slowest").guiText.material.color = Color.white;
		transform.guiText.material.color = Color.red;


		if(transform.name == "Fastest")
		{
			GameObject.Find("Managers").GetComponent<GameSettings>().gameSpeedMultiplier = 4.0f;
		}
		else if(transform.name == "Fast")
		{
			GameObject.Find("Managers").GetComponent<GameSettings>().gameSpeedMultiplier = 2.0f;
		}
		else if(transform.name == "Normal")
		{
			GameObject.Find("Managers").GetComponent<GameSettings>().gameSpeedMultiplier = 1.0f;
		}
		else if(transform.name == "Slow")
		{
			GameObject.Find("Managers").GetComponent<GameSettings>().gameSpeedMultiplier = 0.5f;
		}
		else if(transform.name == "Slowest")
		{
			GameObject.Find("Managers").GetComponent<GameSettings>().gameSpeedMultiplier = 0.25f;
		}
	}
}
