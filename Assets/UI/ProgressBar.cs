using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {
	float progress;
	Vector2 pos;
	Vector2 size;
	private Texture2D progressBarEmpty;

	public Texture2D progressBarFull;
	private AudioSource mainAudio;
	private float audioLength;
	private bool isPaused;
	GameObject PauseButton;
	private bool mouseDown;
	private Rect progressRect;
	MainCharacterBehavior mainCharBehavior;
	bool wasPaused;
	GameSettings gameSettings;

	private float KeySongIncreaseInSeconds = 0.25f;
	private bool IsScrubbing = false;


	void Awake()
	{
		progress = 0;
		pos = new Vector2(130,550);
		size = new Vector2(600,75);

		mainAudio = GameObject.Find("Managers").GetComponent<AudioSource>();
		mainCharBehavior  = GameObject.Find("MainCharacter").GetComponent<MainCharacterBehavior>();
		audioLength = mainAudio.clip.length;

		isPaused = mainAudio.playOnAwake;

		PauseButton = GameObject.Find("PauseStartButton");

		mouseDown = false;

		progressRect = new Rect(pos.x, pos.y, size.x, size.y);

		CreateTexture();

		gameSettings = GameObject.Find("Managers").GetComponent<GameSettings>();
	}

	void CreateTexture()
	{
		Color[] blank;

		progressBarEmpty = new Texture2D((int)size.x, (int)size.y);

		// Grab the shit
		int audioSize = mainAudio.clip.samples * mainAudio.clip.channels;
		float[] samples = new float[audioSize];
		// create a 'blank screen' image
		blank = new Color[(int)size.x * (int)size.y];
		
		for(var i = 0; i<blank.Length; i++)
		{
			blank[i] = Color.black;  
		}

		mainAudio.clip.GetData(samples, 0);
		// clear the texture
		progressBarEmpty.SetPixels(blank, 0);
		// draw the waveform
		for (int i = 0; i < audioSize; i++){
			progressBarEmpty.SetPixel((int)(size.x * i / audioSize), (int)(size.y * (samples[i]+1f)/2), Color.white);
		}
		// upload to the graphics card
		progressBarEmpty.Apply();

	}
	void OnGUI()
	{
		GUI.DrawTexture(progressRect, progressBarEmpty);
		GUI.DrawTexture(new Rect(pos.x, pos.y, size.x * Mathf.Clamp01(progress), size.y), progressBarFull);

		bool mouseJustDown = false;
		bool mouseJustUp = false;
		IsScrubbing = false;
		Event currentEvent = Event.current;
		switch (currentEvent.button) {
		case 0:
			switch (currentEvent.type) {
			case EventType.MouseDown:
				mouseJustDown = true;
				break;
			case EventType.MouseUp:
				mouseJustUp = true;
				break;
			}
			break;
		}

		if(mouseJustUp)
		{
			mouseDown = false;

			if(isPaused)
			{
				mainAudio.Pause();
			}
		}
		else if(mouseJustDown)
		{
			// Check location
			if(progressRect.Contains(currentEvent.mousePosition))
			{
				mouseDown = true;
				SetAudioTime(currentEvent.mousePosition);
				mainAudio.Play();
				IsScrubbing = true;
			}
		}
		else if(mouseDown)
		{
			// Update progress with mousepos
			SetAudioTime(currentEvent.mousePosition);
			IsScrubbing = true;
		}
	} 

	void SetAudioTime(Vector2 currentPos)
	{
		float progressRatio = (currentPos.x - progressRect.xMin) / progressRect.width;
		progressRatio = Mathf.Clamp(progressRatio, 0.0f, 1.0f);
		mainAudio.time = progressRatio * audioLength;
	}

	void Update()
	{
		if(isPaused)
		{
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				mainAudio.time = Mathf.Max(0.0f, mainAudio.time - KeySongIncreaseInSeconds * gameSettings.gameSpeedMultiplier);
			}
			else if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				mainAudio.time = Mathf.Min(audioLength, mainAudio.time + KeySongIncreaseInSeconds * gameSettings.gameSpeedMultiplier);
			}
		}

		mainAudio.pitch = gameSettings.gameSpeedMultiplier;
		progress = mainAudio.time / audioLength;
		mainCharBehavior.UpdatePositionAtTime(mainAudio.time);
	}

	public float GetTime()
	{
		return mainAudio.time;
	}

	public bool IsPaused() { return isPaused; } 

	public bool IsDoingScrubbing(){ return IsScrubbing;  }
	public void PauseEverything(bool shouldPropagate = true)
	{
		if(mainAudio)
		{
			mainAudio.Pause();
		}

		isPaused = true;

		if(shouldPropagate)
		{
			PauseButton.SendMessage("PauseEverything", false);
		}
	}

	public void UnpauseEverything(bool shouldPropagate = true)
	{
		isPaused = false;

		if(mainAudio)
		{
			mainAudio.Play();
		}

		if(shouldPropagate)
		{
			PauseButton.SendMessage("UnpauseEverything", false);
		}
	}
}
