﻿using UnityEngine;
using System.Collections;

public class MainCharacterBehavior : MonoBehaviour {

	private float MainCharSpeed = 5.0f;

	private Vector3 startPos;

	float delta = 1/30.0f;
	// Use this for initialization
	void Start () {
		startPos = transform.position;
	}

	void Awake()
	{
		QualitySettings.vSyncCount = 2;
		Application.targetFrameRate = 30;
	}
	// Update is called once per frame
	void Update () {
		//transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + delta*MainCharSpeed);
	}

	public void UpdatePositionAtTime(float time)
	{
		float numUpdates = time / delta;
		
		transform.position = new Vector3(startPos.x, startPos.y, startPos.z + (numUpdates * delta*MainCharSpeed));
	}
}
