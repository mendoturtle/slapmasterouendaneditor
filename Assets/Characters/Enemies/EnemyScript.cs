using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

	public AudioClip SucceedAudio;
	private bool isSucceeded = false;
	private Color selfColor;
	private float offset = 0.5f;
	private EnemyManager enemyManager;
	private GameObject mainCharacter;

	private bool IsSelected = false;

	private AudioSource audioSource;
	public int EnemyType = 0;  // 1 TAP  2  HORIZONTAL  3 VERTICAL

	// Use this for initialization
	void Start () {
		mainCharacter = GameObject.Find("MainCharacter");
		selfColor = gameObject.renderer.material.color;

		enemyManager = GameObject.Find("Managers").GetComponent<EnemyManager>();
		enemyManager.AddEnemy(gameObject);

		audioSource = GameObject.Find("Managers").GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
		if((gameObject.transform.parent.transform.position.z - offset) <= mainCharacter.transform.position.z)
	 	{
			if(!isSucceeded)
			{
				if(!GameObject.Find("ProgressBarObj").GetComponent<ProgressBar>().IsPaused()
				   && !GameObject.Find("ProgressBarObj").GetComponent<ProgressBar>().IsDoingScrubbing())
				{
					audioSource.PlayOneShot(SucceedAudio);
				}

				isSucceeded = true;
			}

			gameObject.renderer.material.color = Color.white;
	 	}
	 	else
		{
			gameObject.renderer.material.color = selfColor;
			isSucceeded = false;
		}

		if(IsSelected)
		{
			if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Delete))
			{
				enemyManager.RemoveEnemy(gameObject.transform.parent.gameObject);
			}
			gameObject.renderer.material.color = Color.yellow;
		}
	}

	public void SetSelected(bool selected)
	{
		IsSelected = selected;
	}
}
