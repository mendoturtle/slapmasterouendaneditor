﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class BrowserLevels : MonoBehaviour {

	public Browser Browser;
	private string exTension = ".txt";

	public string lastFile;
	private bool isDirty = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnGUI()
	{
		if(!Browser)
			return;
		// To Open a file with the browser run:  Browser.OpenFile( The_Path_To_Your_MainSaves_Folder );
		// The following is an example that will open the root directory of your project. If you run this in
		// Unity, without compiling, then the file browser will be a duplicate of your "Project" tab.

		if(GUI.Button(new Rect(5,125,190,30), "Open Level"))
		{
			Browser.SetExtensionType(exTension);
			Browser.OpenFile( Application.dataPath + "/Data/Levels/");			
		}
		
		// Since opening a file is reliant on the user making multiple actions before the file they wish to open is actually
		// known to the computer, I am unsure of how to pass references from "Browser" to any potential code "Browser" could be used with.
		// As such I can't make it as simple for you to do things like: var FileToOpen = Browser.OpenFile( SaveFiles );
		// because "Browser" has to wait for the user to choose the file they wish to open before "FileToOpen" could even be set.
		// This means that when you are setting up "Browser" to actually be used in a game, you must add your own file opening code to "Browser" first.
		// I have added a large comment block in "Browser" to explain how to do that. You however do not need to do add anything for the purpose
		// of this demo, instead "Browser" will Debug the path the user opens in the console so that you can verify that it is working.
		// 
		// The code that you add to open your file will be completely dependent on what you need to load. By using "Browser" I'm assuming
		// that you already have some method of saving and loading data to some form of file on disk (Player Prefs won't work with "Browser")
		// and if you do all you should need to do is call your "Load File" function where I commented in "Browser" and pass it the directory 
		// that "Browser" will supply.


		// To Save a file with Browser you must first create a file on the disk in a temporary location. Then when you call "Browser.SaveFile"
		// "Browser" simply moves the file you pass it to where ever the user selects, if the user cancels, then the file is deleted. In any 
		// case you must first save the file somewhere before calling "Browser.SaveFile", this is simply to make integrating "Browser" easier,
		// eventually I hope to make the OpenFile function just as "plug'n'play" and then "Browser" would be usable in multiple scenarios in one 
		// game without needing modification.
		//
		// To test "Browser.SaveFile" I'm just going to display a text field on the screen, save it via .Net, and pass it to "Browser". If you 
		// don't know any thing about .Net, don't worry, all you need to use "Browser.SaveFile" is the directory of a file to save, and a directory
		// that you want to restrict the user to (so they don't rome the whole dang hard drive, although you could restrict them to the hard drive 
		// it self, and then "Browser" would basically be a second file browser for their whole computer). However, for the purpose of this demo, 
		// the user will be restricted to the folder I've supplied "Demo Directory Tree".
		//
		// Note: You probably shouldn't save the temporary file inside the directory your restricting the user to, doing so would mean that they'd
		// be able to see the temporary file, it won't prevent any thing from working, it would just be awkward for the user.

		if(isDirty)
		{
			GUI.color = Color.yellow;
		}

		if(GUI.Button(new Rect(5,160,190,30), "Save Level"))
		{					
			Browser.SetExtensionType(exTension);
			// All of this is just to create an example text file, if you want to know more about how I created it with System.IO and .Net 
			// feal free contact me with any questions on the forums (I know from experiance that there isn't exaclty much documentation 
			// on saving and loading files for beginers out there), or do what I did and comb through the .Net library's refrence on System.IO 
			// at:   http://msdn.microsoft.com/en-us/library/system.io.aspx
			
			string FileName = Application.dataPath + "/TempSaveFile";
			
			SaveLevel(FileName);
			
			// This is the actual save file comand that you need to know about, the first:
			Browser.SaveFile( FileName, Application.dataPath+"/Data/Levels/");
		}
	}

	public void OpenLevel(string FileToOpen)
	{
		GameObject.Find("Managers").GetComponent<EnemyManager>().RemoveAllEnemies();

		StreamReader reader = new StreamReader(FileToOpen);
		string difficulty = reader.ReadLine(); // ignore difficulty
		int numEnemies = int.Parse(reader.ReadLine());

		for(int i = 0; i < numEnemies; ++i)
		{
			float newX = System.Single.Parse(reader.ReadLine());
			float newY = 0.0f;
			float newZ = System.Single.Parse(reader.ReadLine());
			Vector3 newPos = new Vector3();
			newPos.x = newX;
			newPos.y = newY;
			newPos.z = newZ;

			int enemyType = int.Parse(reader.ReadLine());
			string separator = reader.ReadLine(); // separator line

			GameObject.Find("Managers").GetComponent<EnemyManager>().CreateEnemy(enemyType, newPos);
		}

		reader.Close();
		reader.Dispose();

		isDirty = false;
		lastFile = FileToOpen;
	}
	
	void SaveLevel(string FileName)
	{
		System.IO.File.WriteAllText(FileName,string.Empty);

		StreamWriter Sw = new StreamWriter(FileName);

		// Write dificulty 
		Sw.WriteLine("0");

		// write song
		//Sw.WriteLine(GameObject.Find("Managers").GetComponent<AudioSource>().clip.name);

		// write enemies
		List<GameObject> enemies = GameObject.Find("Managers").GetComponent<EnemyManager>().enemies;
		Sw.WriteLine(enemies.Count);

		Debug.Log("YO YO - " + enemies.Count);
		for(int i = 0; i < enemies.Count; ++i)
		{
			// format X Y TYPE
			Sw.WriteLine(enemies[i].transform.position.x);
			Sw.WriteLine(enemies[i].transform.position.z);
			Sw.WriteLine(enemies[i].GetComponentInChildren<EnemyScript>().EnemyType);
			Sw.WriteLine("-------------");
		}

		Sw.Close();
	}

	public void SaveDone(string FileName)
	{
		if(!string.IsNullOrEmpty(FileName))
		{
			isDirty = false;
			lastFile = FileName;
		}
	}

	public void SetDirty()
	{
		isDirty = true;
	}
}
