﻿using UnityEngine;
using System.Collections;

public class Gizmo : MonoBehaviour {
	private GizmoHandle axisX;
	private GizmoHandle axisY;
	private GizmoHandle axisZ;

	public GameObject axisXGameObj;
	public GameObject axisYGameObj;
	public GameObject axisZGameObj;
	
	public GizmoMyType gizmoType;
	
	public bool  needUpdate = false;

	public void GatherData()
	{
		axisX = axisXGameObj.GetComponent<GizmoHandle>();
		axisY = axisYGameObj.GetComponent<GizmoHandle>();
		axisZ = axisZGameObj.GetComponent<GizmoHandle>();
		
		axisX.axis = GizmoAxis.X;
		axisY.axis = GizmoAxis.Y;
		axisZ.axis = GizmoAxis.Z;
		
		setType(gizmoType);
	}

	void  Start (){

	}
	
	
	void  Update (){    
		needUpdate = (axisX.needUpdate || axisY.needUpdate || axisZ.needUpdate);
	}
	
	public void  setType ( GizmoMyType type  ){
		axisX.setType(type);
		axisY.setType(type);
		axisZ.setType(type);
	}
	
	public void  setParent ( Transform other  ){
		transform.parent = other;
		axisX.setParent(other);
		axisY.setParent(other);
		axisZ.setParent(other);
	}
	
}