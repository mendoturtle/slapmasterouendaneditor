﻿using UnityEngine;
using System.Collections;

public class Gizmonizer : MonoBehaviour {
	public GameObject gizmoAxis;
	public float gizmoSize = 1.0f;
	
	private GameObject gizmoObj = null;
	private Gizmo gizmo = null;
	private GizmoMyType gizmoType = GizmoMyType.Position;
	
	void  Update (){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			removeGizmo();
		}

		if (gizmo != null) {
			if (Input.GetKeyDown(KeyCode.Alpha1)) {
				gizmoType = GizmoMyType.Position;
				gizmo.setType(gizmoType);
			}
			if (Input.GetKeyDown(KeyCode.Alpha2)) {
				gizmoType = GizmoMyType.Rotation;
				gizmo.setType(gizmoType);
			}
			if (Input.GetKeyDown(KeyCode.Alpha3)) {
				gizmoType = GizmoMyType.Scale;
				gizmo.setType(gizmoType);
			}        
			if (gizmo.needUpdate) {
				resetGizmo();
			}
		}
		else
		{
			if (Input.GetMouseButtonDown(0)){
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit)){
					
					if(hit.collider.transform.gameObject == gameObject)
					{
						RemoveAll ();
						
						if(!gizmoObj){
							resetGizmo();
						}
					}
				}
				else
				{
					RemoveAll ();
				}
			}
		}
	}

	private void RemoveAll()
	{
		Gizmonizer[] gizmoList = FindObjectsOfType(typeof(Gizmonizer)) as Gizmonizer[];
		foreach(Gizmonizer gizmoIter in gizmoList) {
			gizmoIter.removeGizmo();
		}
		
		EnemyScript[] enemyList = FindObjectsOfType(typeof(EnemyScript)) as EnemyScript[];
		foreach(EnemyScript enemy in enemyList) {
			enemy.SetSelected(false);
		}
	}
	
	public void  removeGizmo (){
		if (gizmoObj) {
			gameObject.layer = 0;
			foreach(Transform child in transform) {
				child.gameObject.layer = 0;
			}   

			EnemyScript[] enemyScripts;
			enemyScripts = GetComponentsInChildren<EnemyScript>();
			foreach(EnemyScript enemyScript in enemyScripts) {
				enemyScript.SetSelected(false);
			}

			Destroy(gizmoObj);    
			Destroy(gizmo);    
		}
	}
	
	public void  resetGizmo (){
		removeGizmo();
		gameObject.layer = 2;
		foreach(Transform child in transform) {
			child.gameObject.layer = 2;
		}        
		gizmoObj = Instantiate(gizmoAxis, transform.position, transform.rotation) as GameObject;
		gizmoObj.transform.localScale *= gizmoSize;
		gizmo = gizmoObj.GetComponent<Gizmo>();
		gizmo.GatherData();
		gizmo.setParent(transform);
		gizmo.setType(gizmoType);
		
		EnemyScript[] enemyScripts;
		enemyScripts = GetComponentsInChildren<EnemyScript>();
		foreach(EnemyScript enemyScript in enemyScripts) {
			enemyScript.SetSelected(true);
		}
		
	}
	
}