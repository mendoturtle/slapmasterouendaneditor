﻿using UnityEngine;
using System.Collections;

public enum GizmoControl {Horizontal, Vertical, Both}
public enum GizmoMyType {Position, Rotation, Scale} 
public enum GizmoAxis {X, Y, Z}

public class GizmoHandle : MonoBehaviour {
	
	public GameObject positionEnd;
	public GameObject rotationEnd;
	public GameObject scaleEnd;
	public float moveSensitivity = 2;
	public float rotationSensitivity = 64;
	public bool  needUpdate = false;

	
	public GizmoMyType type = GizmoMyType.Position;
	public GizmoControl control = GizmoControl.Both;
	public GizmoAxis axis = GizmoAxis.X;
	
	private bool  mouseDown = false;
	private Transform otherTrans;
	private float selfDelta= 1/30.0f;
	private float dragSpeed= 5.0f;
	void  Awake (){
		otherTrans = transform.parent;
	}
	
	void  Update (){
		
	}
	
	public void  setParent ( Transform other  ){
		otherTrans = other;    
	}
	
	public void  setType ( GizmoMyType type  ){
		this.type = type;
		positionEnd.SetActive(type == GizmoMyType.Position);
		rotationEnd.SetActive(type == GizmoMyType.Rotation);
		scaleEnd.SetActive(type == GizmoMyType.Scale);
	}
	
	void  OnMouseDown (){
		mouseDown = true;
	}
	
	void  OnMouseUp (){
		mouseDown = false;
		needUpdate = true;
	}
	
	
	void  OnMouseDrag (){
		float delta = 0.0f;
		if (mouseDown) {
			switch (control) {
			case GizmoControl.Horizontal:
				delta = Input.GetAxis("Mouse X") * selfDelta * dragSpeed; 
				break;
			case GizmoControl.Vertical:
				delta = Input.GetAxis("Mouse Y") * selfDelta * dragSpeed; 
				break;
			case GizmoControl.Both:
				delta = (Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * selfDelta * dragSpeed; 
				break;
			}
			
			delta *= moveSensitivity;

			if(type ==  GizmoMyType.Position)
			{
				switch (axis) {
				case GizmoAxis.X:
					otherTrans.Translate(Vector3.right * delta);
					GameObject.Find("Browser/Browser_Levels").GetComponent<BrowserLevels>().SetDirty();
					break;
				case GizmoAxis.Y:
					otherTrans.Translate(Vector3.up * delta);
					GameObject.Find("Browser/Browser_Levels").GetComponent<BrowserLevels>().SetDirty();
					break;
				case GizmoAxis.Z:
					otherTrans.Translate(Vector3.forward * delta);
					GameObject.Find("Browser/Browser_Levels").GetComponent<BrowserLevels>().SetDirty();
					break;
				}
			}
		}
	}
	
	
	
}